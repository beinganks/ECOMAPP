import { NgModule } from '@angular/core';
import { RouterModule,Routes} from '@angular/router';
import { ProductListItemComponent } from './product/components/product-list-item/product-list-item.component';
import { ProductListComponent} from './product/components/product-list/product-list.component';
import { CartListComponent} from './cart/components/cart-list/cart-list.component';
import {ProductDetailsComponent} from './product/components/product-details/product-details.component';
import { LoginComponent } from './auth/components/login/login.component';
const routes:Routes =[
  {
   path:'login',
   component:LoginComponent
  },
  {
    path:'products',
    component:ProductListComponent
  },
  {
    path:'cart',
    component:CartListComponent
  },
  {
    path:'',
    redirectTo:'/products',
    pathMatch:'full'
  },
  {
    path:'Product-details/:ProductID',
    component:ProductDetailsComponent

  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)
    
  ],
  exports:[RouterModule],
  declarations: []
})
export class AppRoutingModule { }
