import { Component, OnInit } from '@angular/core';
import { AppService } from '../app/app.service';
import { AuthService } from './auth/auth.service';
import {Request,Response} from '@angular/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoggedIn: boolean;
  activeView="ProductList";
  title = 'app';
  setView(view)
  {
  this.activeView=view;
  }
  constructor(private service:AppService,private authService:  AuthService)
{
  
}

ngOnInit(){

  this.service.viewChangeEvent.subscribe(viewName =>{
    this.activeView =viewName;
  });
  this.authService.loginChangeEvent.subscribe(isLoggedIn =>{
    this.isLoggedIn = isLoggedIn;
  });
  // this.Http.get(this.URL)
  //    .map(response:Response) =>Response.json())
  //    .subscribe((result:any) =>{
  //      this.cart= result;
  //    }
  // }
  
  
  
  


}
 onLogout()
 {
   this.authService.logout();
 }
}