import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private service:AuthService) { }

  ngOnInit() {
  }
 onLoginSubmit(myForm:NgForm)
 {
   if(myForm.valid )
   {
     this.service.login(myForm.value);
}
else{
  alert("invalid");
}
}
}
