import { Injectable } from '@angular/core';
import { Router} from '@angular/router';
import IUser from '../Models/users';
import { Subject } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
   
  private  users=[
  {
email:'ankit.sachdev93@gmail.com',
password:'ankit123'
},
{
  email:'ankit.sachdev@acheron-tech.com',
  password:'ankit123'
}
  ]
  constructor(private router:Router) { }
public loginChangeEvent:Subject<boolean>=new Subject<boolean>();

login(userInfo:IUser)
{
  const res = this.users.find((u:IUser) => {
    return u.email === userInfo.email && u.password === userInfo.password;
  });
  if(res){
    sessionStorage.setItem('userEmail',"true");
    this.loginChangeEvent.next(true);
    this.router.navigate(['/']);
    return true;
}
return false;
}
isLoggedIn():boolean
{
 const userEmail=sessionStorage.getItem('userEmail');
 return userEmail ?true:false;
}
logout():void
{
  sessionStorage.clear();
  this.loginChangeEvent.next(false);

}
}
