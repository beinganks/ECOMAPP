import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinicartComponent } from './components/minicart/minicart.component';
import { CartListComponent } from './components/cart-list/cart-list.component';
import { CheckOutComponent } from './components/check-out/check-out.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MinicartComponent,
     CartListComponent,
      CheckOutComponent],
   exports:[
     MinicartComponent,
     CartListComponent
   ]

    })
export class CartModule { }
