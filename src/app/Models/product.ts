export default interface IProduct
{
    id:string,
    name:string;
    company:string;
    description:string;
    price:number;
    imgUrl:string;
}