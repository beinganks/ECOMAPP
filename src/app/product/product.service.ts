import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import IProduct from '../Models/product';
import {map} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http:HttpClient) { }

getProducts():Observable<IProduct[]>
  {
  return this.http.get<IProduct[]>('assets/Data/products.json');
}


getProductsById(productID:string): Observable <IProduct>{
  return this.http.get<IProduct>('assets/Data/products.json')
  .pipe(
    map((products:IProduct[]) => {
      return products.find((Prod:IProduct) =>Prod.id ===productID)
    })
  )
}}


