import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../../product.service';
import { Observable } from 'rxjs';
import IProduct from '../../../Models/product';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
Products$:Observable<IProduct[]>;
constructor(public abc:ProductService){}

  ngOnInit() {
      this.Products$=this.abc.getProducts();
    };
  }

