import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import { ProductService } from '../../product.service';
import IProduct from '../../../Models/product';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
ProductDetail:IProduct
  constructor(private route:ActivatedRoute,private service:ProductService) { }

  ngOnInit() {
const ProductId=this.route.snapshot.paramMap.get('ProductID');
this.service.getProducts().subscribe((products: IProduct[] ) => {
  this.ProductDetail=products.find(Prod => Prod.id ===ProductId);

});
console.log(ProductId);
  
// this.route.paramMap.subscribe(param => 
//    {
//   console.log(param.get('ProductId'));
// });

  }

}
