import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductListItemComponent } from './components/product-list-item/product-list-item.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AppRoutingModule } from '../app-routing.module';
@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  declarations: [ProductListComponent, ProductListItemComponent, ProductDetailsComponent],
  exports:[
    ProductListComponent,
    ProductDetailsComponent,
    AppRoutingModule
  ]
})
export class ProductModule { }
